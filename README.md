# OpenML dataset: Are-Two-Sentences-of-the-Same-Topic

https://www.openml.org/d/43691

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Do two sentences come from the same article?  We randomly sampled sentences from across Wikipedia.  Some sentences came from the same articles, others do not.  
Sentences from the Same Article
These two sentences are from the same article.

There were 2,788 housing units at an average density of 4 per squaremile (2/km).    
It is also home to the Oklahoma State Reformatory, located in Granite.

So are these:

Monument of the Judiciary Citadel of Salerno, near the Colle Bellara.    
The La Carnale Castle got his name from a medieval battle against the Arabs and is part of a sport complex (with pool, tennis courts and hockey).

As are these:

The idea of Haar measure is to take a sort of limit of  as  becomes smaller to make it additive on all pairs of disjoint compact sets, though it first has to be normalized so that the limit is not just infinity.    
When left and right Haar measures differ, the right measure is usually preferred as a prior distribution.

Sentences from Different Articles
These two sentences are from different articles:

US Open womens doubles champion    France     Ranked world No.    
The average household size was 2.72 and the average family size was 3.19.

As are these:

The initial goal of the WordNet project was to build a lexical database that would be consistent with theories of human semantic memory developed in the late 1960s.    
Males had a median income of 25,625 versus 20,515 for females.

These are also different:

Meanwhile, Western foods which are rich in fat, salt, sugar, and refined starches are also imported into countries.    
According to the United States Census Bureau, the CDP has a total area of , of which  is land and  (3.61) is water.

Disclaimer
Please note, we attempted to remove any data sampled that includes controversial words or hate speech.  However, such language is present in Wikipedia, so some such material may be present in this dataset. Due to the size of this dataset, it was not possible to have a human being audit each sentence.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43691) of an [OpenML dataset](https://www.openml.org/d/43691). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43691/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43691/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43691/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

